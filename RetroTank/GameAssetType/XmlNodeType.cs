﻿
namespace GameAssetType
{
    public enum XmlNodeType
    {
        Element,
        Comment,
        Text
    }
}
