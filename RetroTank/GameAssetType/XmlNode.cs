﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GameAssetType
{
    [Serializable]
    public class XmlNode
    {

        #region Properties

        public string Name
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }

        public XmlNodeType NodeType
        {
            get;
            set;
        }

        public string InnerText
        {
            get
            {
                switch (NodeType)
                {
                    case XmlNodeType.Element:
                        return ChildNodes.Aggregate(string.Empty, (current, childNode) => current + childNode.InnerText);
                    case XmlNodeType.Comment:
                        return Value;
                    case XmlNodeType.Text:
                        return Value;
                    default:
                        return Value;
                }
            }
        }

        public List<XmlNode> ChildNodes
        {
            get;
            private set;
        }

        public List<XmlAttribute> Attributes
        {
            get;
            private set;
        }

        #endregion

        public XmlNode()
        {
            Name = string.Empty;
            Value = string.Empty;
            ChildNodes = new List<XmlNode>();
            Attributes = new List<XmlAttribute>();
        }

        #region Public methods

        public XmlAttribute GetAttribute(string name)
        {
            return Attributes.FirstOrDefault(attribute => attribute.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
        }

        public T ConvertTo<T>()
        {
            var result = Activator.CreateInstance<T>();

            return SetProperties(result, this);
        }

        #endregion

        private static T SetProperties<T>(T obj, XmlNode node)
        {
            if (obj == null)
            {
                return default(T);
            }

            var type = obj.GetType();

            MemberInfo[] members;

            // Find corresponding field in object by node attributes
            foreach (var attribute in node.Attributes)
            {
                members = type.GetMember(attribute.Name);
                foreach (var memberInfo in members)
                {
                    // Found in attribute list
                    if (memberInfo is FieldInfo)
                    {
                        (memberInfo as FieldInfo).SetValue(obj, ConvertTo((memberInfo as FieldInfo).FieldType, attribute.Value));
                    }
                    else if (memberInfo is PropertyInfo)
                    {
                        (memberInfo as PropertyInfo).SetValue(obj, ConvertTo((memberInfo as PropertyInfo).PropertyType, attribute.Value), null);
                    }
                }
            }

            // Find corresponding field in object by node name
            members = type.GetMember(node.Name);
            foreach (var memberInfo in members)
            {
                // This Xml node has the same name with property
                if (memberInfo is FieldInfo)
                {
                    (memberInfo as FieldInfo).SetValue(obj, ConvertTo((memberInfo as FieldInfo).FieldType, node));
                }
                else if (memberInfo is PropertyInfo)
                {
                    (memberInfo as PropertyInfo).SetValue(obj, ConvertTo((memberInfo as PropertyInfo).PropertyType, node), null);
                }
            }

            // Find in child nodes
            obj = node.ChildNodes.Aggregate(obj, SetProperties<T>);

            return obj;
        }

        private static object ConvertTo(Type type, XmlNode node)
        {
            if (type.IsArray || (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>)))
            {
                var elementType = type.IsArray ? type.GetElementType() : type.GetGenericArguments()[0];

                Array array;
                if (elementType.IsPrimitive || elementType == typeof(string))
                {
                    var length = 0;
                    foreach (var childNode in node.ChildNodes)
                    {
                        // Array of Primitive type can be break by space, tab, or new line
                        if (elementType.IsPrimitive)
                        {
                            var items = childNode.InnerText.Split(new char[]
                                                              {
                                                                  ' ', '\n', '\r', '\t'
                                                              },
                                                          StringSplitOptions.RemoveEmptyEntries);
                            length += items.Length;
                        }
                        else
                        {
                            var items = childNode.InnerText.Split(new char[]
                                                              {
                                                                  '\n',
                                                              },
                                                          StringSplitOptions.RemoveEmptyEntries);
                            length += items.Length;
                        }
                    }

                    array = Array.CreateInstance(elementType, length);

                    var idx = 0;
                    foreach (var childNode in node.ChildNodes)
                    {
                        // Array of Primitive type can be break by space, tab, or new line
                        if (elementType.IsPrimitive)
                        {
                            var items = childNode.InnerText.Split(new char[]
                                                              {
                                                                  ' ', '\n', '\r', '\t'
                                                              },
                                                          StringSplitOptions.RemoveEmptyEntries);
                            foreach (var item in items)
                            {
                                array.SetValue(ConvertTo(elementType, item), idx);
                                ++idx;
                            }
                        }
                        else
                        {
                            var items = childNode.InnerText.Split(new char[]
                                                              {
                                                                  '\n',
                                                              },
                                                          StringSplitOptions.RemoveEmptyEntries);
                            foreach (var item in items)
                            {
                                array.SetValue(ConvertTo(elementType, item), idx);
                                ++idx;
                            }
                        }
                    }
                }
                else
                {
                    array = Array.CreateInstance(elementType, node.ChildNodes.Count);

                    for (var idx = 0; idx < node.ChildNodes.Count; idx++)
                    {
                        var item = ConvertTo(elementType, node.ChildNodes[idx]);
                        array.SetValue(item, idx);
                    }
                }

                if (type.IsArray)
                {
                    return array;
                }
                else
                {
                    var listType = (typeof(List<>)).MakeGenericType(new Type[]
                                                                     {
                                                                         elementType
                                                                     });

                    return Activator.CreateInstance(listType, array);
                }
            }
            else if (type.IsClass && type != typeof(string))
            {
                var item = Activator.CreateInstance(type);
                return SetProperties(item, node);
            }
            else
            {
                return ConvertTo(type, node.InnerText);
            }
        }

        private static object ConvertTo(Type type, string value)
        {
            if (type.IsPrimitive)
            {
                return Convert.ChangeType(value, type, null);
            }
            else if (type.IsEnum)
            {
                return Enum.Parse(type, value, true);
            }
            else if (type == typeof(string))
            {
                return value;
            }
            else
            {
                var parameters = value.Split(new char[]
                                                 {
                                                     ' ', '\n', '\r', '\t'
                                                 }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var constructor in type.GetConstructors())
                {
                    if (constructor.GetParameters().Length == parameters.Length)
                    {
                        var typedParameters = new object[parameters.Length];
                        for (var idx = 0; idx < constructor.GetParameters().Length; idx++)
                        {
                            typedParameters[idx] = ConvertTo(constructor.GetParameters()[idx].ParameterType, parameters[idx]);
                        }
                        return constructor.Invoke(typedParameters);
                    }
                }
            }

            return value;
        }
    }
}
