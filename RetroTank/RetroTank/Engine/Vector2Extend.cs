﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RetroTank.Engine
{
    public static class Vector2Extend
    {
        /// <summary>
        /// Shift a vector a horizontal distance
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public static Vector2 ShiftX(this Vector2 vector, float x)
        {
            return new Vector2(vector.X + x, vector.Y);
        }

        /// <summary>
        /// Shift a vector a vertical distance
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static Vector2 ShiftY(this Vector2 vector, float y)
        {
            return new Vector2(vector.X, vector.Y + y);
        }

        /// <summary>
        /// Convert Point to Vector2
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Vector2 ToVector2(this Point point)
        {
            return new Vector2(point.X, point.Y);
        }
    }
}
