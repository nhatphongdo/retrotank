﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RetroTank.Engine
{
    public static class Utility
    {
        /// <summary>
        /// Check if a box is totally inside a container
        /// </summary>
        /// <param name="box"></param>
        /// <param name="boundary"></param>
        /// <returns></returns>
        public static bool IsInside(Rectangle box, Rectangle boundary)
        {
            return (box.X >= boundary.X && box.X + box.Width <= boundary.X + boundary.Width
                && box.Y >= boundary.Y && box.Y + box.Height <= boundary.Y + boundary.Height);
        }

        /// <summary>
        /// Get the boundary from position and size
        /// </summary>
        /// <param name="position"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Rectangle GetBoundary(Vector2 position, Vector2 size)
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
        }

        /// <summary>
        /// Check collision between two object
        /// </summary>
        /// <param name="box1"></param>
        /// <param name="box2"></param>
        /// <returns></returns>
        public static bool IsCollision(Rectangle box1, Rectangle box2)
        {
            return box1.Intersects(box2);
        }

        /// <summary>
        /// Check if value is between a specific range
        /// </summary>
        /// <param name="value"></param>
        /// <param name="min"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static bool IsBetween(float value, float min, float length)
        {
            return value >= min && value <= min + length;
        }
    }
}
