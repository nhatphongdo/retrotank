﻿
using Microsoft.Xna.Framework;

namespace RetroTank.Engine.Effect
{
    public class Particle
    {
        // Position, Velocity, and Acceleration represent exactly what their names
        // indicate. They are public fields rather than properties so that users
        // can directly access their .X and .Y properties.
        public Vector2 Position;
        public Vector2 Velocity;
        public Vector2 Acceleration;

        // how long this particle will "live"
        public float Lifetime
        {
            get;
            set;
        }

        // how long it has been since initialize was called
        public float TimeSinceStart
        {
            get;
            set;
        }

        // the scale of this particle
        public float Scale
        {
            get;
            set;
        }

        // its rotation, in radians
        public float Rotation
        {
            get;
            set;
        }

        // how fast does it rotate?
        public float RotationSpeed
        {
            get;
            set;
        }

        // is this particle still alive? once TimeSinceStart becomes greater than
        // Lifetime, the particle should no longer be drawn or updated.
        public bool Active
        {
            get
            {
                return TimeSinceStart < Lifetime;
            }
        }


        // initialize is called by ParticleSystem to set up the particle, and prepares
        // the particle for use.
        public void Initialize(Vector2 position, Vector2 velocity, Vector2 acceleration,
            float lifetime, float scale, float rotationSpeed)
        {
            // set the values to the requested values
            this.Position = position;
            this.Velocity = velocity;
            this.Acceleration = acceleration;
            this.Lifetime = lifetime;
            this.Scale = scale;
            this.RotationSpeed = rotationSpeed;

            // reset TimeSinceStart - we have to do this because particles will be
            // reused.
            this.TimeSinceStart = 0.0f;

            // set rotation to some random value between 0 and 360 degrees.
            this.Rotation = ParticleHelpers.RandomBetween(0, MathHelper.TwoPi);
        }

        // update is called by the ParticleSystem on every frame. This is where the
        // particle's position and that kind of thing get updated.
        public void Update(float dt)
        {
            Velocity += Acceleration * dt;
            Position += Velocity * dt;

            Rotation += RotationSpeed * dt;

            TimeSinceStart += dt;
        }
    }
}
