
namespace RetroTank.Engine.Manager
{
    public enum MovingDirection
    {
        RotateUp,
        RotateRight,
        RotateDown,
        RotateLeft,
        Up,
        Right,
        Down,
        Left
    }
}
