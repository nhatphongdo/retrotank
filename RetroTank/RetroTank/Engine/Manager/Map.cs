﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RetroTank.Engine.Object;
using System.Linq;

namespace RetroTank.Engine.Manager
{
    public class Map
    {
        #region Properties

        public Vector2 MapSize
        {
            get;
            set;
        }

        public Vector2 TileSize
        {
            get;
            set;
        }

        public List<MapLayer> Layers
        {
            get;
            set;
        }

        public int GroundLayer
        {
            get;
            set;
        }

        public int[] CollisionMap
        {
            get;
            set;
        }

        public int EnemyNumber
        {
            get;
            set;
        }

        public List<Vector2> EnemyStartLocations
        {
            get;
            set;
        }

        public List<Vector2> PlayerStartLocations
        {
            get;
            set;
        }

        public List<Flag> Flags
        {
            get;
            set;
        }

        #endregion

        #region Public methods

        public bool IsCollision(ObjectBase gameObject)
        {
            for (var index = 0; index < CollisionMap.Length; index++)
            {
                if (CollisionMap[index] == 0)
                {
                    continue;
                }

                var x = (index % (int)MapSize.X) * (int)TileSize.X;
                var y = (index / (int)MapSize.X) * (int)TileSize.Y;

                if (Utility.IsCollision(
                    new Rectangle((int)gameObject.Position.X, (int)gameObject.Position.Y, (int)gameObject.Size.X, (int)gameObject.Size.Y),
                    new Rectangle(x, y, (int)TileSize.X, (int)TileSize.Y)))
                {
                    return true;
                }
            }

            var layer = GetLayer(GroundLayer);
            foreach (var tile in layer.Tiles.Where(t => t != null && !t.IsThroughable))
            {
                if (gameObject is Bullet)
                {
                    // Bullet has different collision detection
                    var collisionX1 = (int)MathHelper.Clamp(gameObject.Position.X, tile.Position.X, tile.Position.X + tile.Size.X);
                    var collisionX2 = (int)MathHelper.Clamp(gameObject.Position.X + gameObject.Size.X, tile.Position.X, tile.Position.X + tile.Size.X);
                    var collisionY1 = (int)MathHelper.Clamp(gameObject.Position.Y, tile.Position.Y, tile.Position.Y + tile.Size.Y);
                    var collisionY2 = (int)MathHelper.Clamp(gameObject.Position.Y + gameObject.Size.Y, tile.Position.Y, tile.Position.Y + tile.Size.Y);

                    if (collisionX1 != collisionX2 && collisionY1 != collisionY2 &&
                        (collisionX2 - collisionX1) >= gameObject.Size.X / 2 && (collisionY2 - collisionY1) >= gameObject.Size.Y / 2)
                    {
                        return true;
                    }
                }
                else
                {
                    if (Utility.IsCollision(Utility.GetBoundary(gameObject.Position, gameObject.Size),
                                            Utility.GetBoundary(tile.Position, tile.Size)))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public int GetCollision(ObjectBase gameObject)
        {
            for (var index = 0; index < CollisionMap.Length; index++)
            {
                if (CollisionMap[index] == 0)
                {
                    continue;
                }

                var x = (index % (int)MapSize.X) * (int)TileSize.X;
                var y = (index / (int)MapSize.X) * (int)TileSize.Y;

                if (Utility.IsCollision(
                    new Rectangle((int)gameObject.Position.X, (int)gameObject.Position.Y, (int)gameObject.Size.X, (int)gameObject.Size.Y),
                    new Rectangle(x, y, (int)TileSize.X, (int)TileSize.Y)))
                {
                    return index;
                }
            }

            var layer = GetLayer(GroundLayer);
            for (var index = 0; index < layer.Tiles.Count; index++)
            {
                var tile = layer.Tiles[index];
                if (tile == null || tile.IsThroughable)
                {
                    continue;
                }

                if (gameObject is Bullet)
                {
                    // Bullet has different collision detection
                    var collisionX1 = (int)MathHelper.Clamp(gameObject.Position.X, tile.Position.X, tile.Position.X + tile.Size.X);
                    var collisionX2 = (int)MathHelper.Clamp(gameObject.Position.X + gameObject.Size.X, tile.Position.X, tile.Position.X + tile.Size.X);
                    var collisionY1 = (int)MathHelper.Clamp(gameObject.Position.Y, tile.Position.Y, tile.Position.Y + tile.Size.Y);
                    var collisionY2 = (int)MathHelper.Clamp(gameObject.Position.Y + gameObject.Size.Y, tile.Position.Y, tile.Position.Y + tile.Size.Y);

                    if (collisionX1 != collisionX2 && collisionY1 != collisionY2 &&
                        (collisionX2 - collisionX1) >= gameObject.Size.X / 2 && (collisionY2 - collisionY1) >= gameObject.Size.Y / 2)
                    {
                        return index;
                    }
                }
                else
                {
                    if (Utility.IsCollision(Utility.GetBoundary(gameObject.Position, gameObject.Size), Utility.GetBoundary(tile.Position, tile.Size)))
                    {
                        return index;
                    }
                }
            }

            return -1;
        }

        public MapLayer GetLayer(int index)
        {
            return Layers.FirstOrDefault(l => l.Index == index);
        }

        public List<Vector2> FindShortestPath(Vector2 start, Vector2 end)
        {
            var groundLayer = GetLayer(GroundLayer);
            var graphSize = (int)(groundLayer.LayerSize.X * groundLayer.LayerSize.Y);

            // Construct weight map
            var weightMap = new int[graphSize];

            for (var row = 0; row < (int)groundLayer.LayerSize.Y; row++)
            {
                for (var column = 0; column < (int)groundLayer.LayerSize.X; column++)
                {
                    if (CanGo(row, column))
                    {
                        weightMap[groundLayer.GetIndex(column, row)] = 1;
                    }
                    else
                    {
                        weightMap[groundLayer.GetIndex(column, row)] = 0;
                    }
                }
            }

            var currentIndex = groundLayer.GetIndex((int)start.X, (int)start.Y);

            var previousNode = new int[graphSize];
            var distance = new int[graphSize];
            for (var index = 0; index < graphSize; index++)
            {
                distance[index] = int.MaxValue;
                previousNode[index] = -1;
            }

            distance[currentIndex] = 0;

            var path = new List<int>();

            while (path.Count < graphSize)
            {
                var minLength = int.MaxValue;
                var u = -1;
                for (var i = 0; i < graphSize; i++)
                {
                    if (path.Contains(i) || minLength <= distance[i])
                    {
                        continue;
                    }
                    minLength = distance[i];
                    u = i;
                }

                path.Add(u);

                if (u == -1 || distance[u] == int.MaxValue)
                {
                    break;
                }

                var adjacents = new int[4];
                adjacents[0] = u + 1;
                adjacents[1] = u - 1;
                adjacents[2] = u - (int)groundLayer.LayerSize.X;
                adjacents[3] = u + (int)groundLayer.LayerSize.X;
                foreach (var v in adjacents)
                {
                    if (v >= 0 && v < graphSize && weightMap[v] > 0)
                    {
                        var dis = distance[u] + 1;
                        if (dis < distance[v])
                        {
                            distance[v] = dis;
                            previousNode[v] = u;
                        }
                    }
                }
            }

            var result = new List<Vector2>();
            var destinationIndex = groundLayer.GetIndex((int)end.X, (int)end.Y);
            while (previousNode[destinationIndex] >= 0)
            {
                result.Insert(0, groundLayer.GetPosition(destinationIndex));
                destinationIndex = previousNode[destinationIndex];
            }

            // Add start node
            result.Insert(0, groundLayer.GetPosition(destinationIndex));

            return result;
        }

        public bool CanGo(int row, int column)
        {
            var layer = GetLayer(GroundLayer);

            if (row < 0 || row >= layer.LayerSize.Y || column < 0 || column >= layer.LayerSize.X)
            {
                return false;
            }

            for (var index = 0; index < CollisionMap.Length; index++)
            {
                if (CollisionMap[index] == 0)
                {
                    continue;
                }

                var x = (index % (int)MapSize.X);
                var y = (index / (int)MapSize.X);

                if (column == x && row == y)
                {
                    return false;
                }
            }

            foreach (var t in layer.Tiles.Where(t => t != null && !t.IsThroughable))
            {
                var x = t.Position.X / t.Size.X;
                var y = t.Position.Y / t.Size.Y;
                if (column == (int)x && row == (int)y)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion

        #region Private methods


        #endregion
    }
}
