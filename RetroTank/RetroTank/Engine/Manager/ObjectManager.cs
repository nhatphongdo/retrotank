using System;
using System.Collections.Generic;
using RetroTank.Engine.Object;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RetroTank.Engine.Manager
{
    public static class ObjectManager
    {
        public static List<ObjectBase> GameObjects
        {
            get;
            private set;
        }

        static ObjectManager()
        {
            GameObjects = new List<ObjectBase>();
        }

        #region Public methods

        public static void Update(GameTime gameTime)
        {
            for (var index = 0; index < GameObjects.Count; index++)
            {
                GameObjects[index].Update(gameTime);
            }
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            for (var index = 0; index < GameObjects.Count; index++)
            {
                GameObjects[index].Draw(spriteBatch);

#if COLLISION_DEBUG
                GameObjects[index].DrawRegion(spriteBatch);
#endif
            }
        }

        public static void ProcessKeyboard(KeyboardState keyboardState, KeyboardState previousKeyboardState)
        {
            for (var index = 0; index < GameObjects.Count; index++)
            {
                GameObjects[index].ProcessKeyboard(keyboardState, previousKeyboardState);
            }
        }

        public static bool IsCollision(ObjectBase gameObject)
        {
            for (var index = 0; index < GameObjects.Count; index++)
            {
                if (GameObjects[index] == gameObject)
                {
                    // Does not check itself
                    continue;
                }

                if (GameObjects[index] is Bullet && (GameObjects[index] as Bullet).Tank == gameObject)
                {
                    // Does not check bullets of itself
                    continue;
                }

                if (Utility.IsCollision(Utility.GetBoundary(gameObject.Position, gameObject.Size),
                                        Utility.GetBoundary(GameObjects[index].Position, GameObjects[index].Size)))
                {
                    return true;
                }
            }

            return false;
        }

        public static ObjectBase GetCollision(ObjectBase gameObject)
        {
            for (var index = 0; index < GameObjects.Count; index++)
            {
                if (GameObjects[index] == gameObject)
                {
                    // Does not check itself
                    continue;
                }

                if (GameObjects[index] is Bullet && (GameObjects[index] as Bullet).Tank == gameObject)
                {
                    // Does not check bullets of itself
                    continue;
                }

                if (Utility.IsCollision(Utility.GetBoundary(gameObject.Position, gameObject.Size),
                                        Utility.GetBoundary(GameObjects[index].Position, GameObjects[index].Size)))
                {
                    return GameObjects[index];
                }
            }

            return null;
        }

        #endregion
    }
}
