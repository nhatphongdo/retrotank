﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RetroTank.Engine.Object;

namespace RetroTank.Engine.Manager
{
    public class MapLayer
    {
        #region Properties

        public Map Map
        {
            get;
            set;
        }

        public int Index
        {
            get;
            set;
        }

        public bool Visible
        {
            get;
            set;
        }

        public Vector2 LayerSize
        {
            get;
            set;
        }

        public Vector2 TileSize
        {
            get;
            set;
        }

        public string LayerTexture
        {
            get;
            set;
        }

        public Vector2 Offset
        {
            get;
            set;
        }

        public List<int> Content
        {
            get;
            internal set;
        }

        public List<Tile> Tiles
        {
            get;
            internal set;
        }

        public List<int> TileProperties
        {
            get;
            set;
        }

        #endregion

        public MapLayer()
        {
            Visible = true;
            Content = new List<int>();
            Tiles = new List<Tile>();
            TileProperties = new List<int>();
        }

        #region Public methods

        public Tile GetTile(int col, int row)
        {
            var index = GetIndex(col, row);
            return GetTile(index);
        }

        public Tile GetTile(int index)
        {
            if (index >= 0 && index < Tiles.Count)
            {
                return Tiles[index];
            }

            return null;
        }

        public void UpdateTile(int col, int row, Tile tile)
        {
            var index = GetIndex(col, row);
            UpdateTile(index, tile);
        }

        public void UpdateTile(int index, Tile tile)
        {
            if (index >= 0 && index < Tiles.Count)
            {
                Tiles[index] = tile;
            }
        }

        public void RemoveTile(int col, int row)
        {
            var index = GetIndex(col, row);
            RemoveTile(index);
        }

        public void RemoveTile(int index)
        {
            if (index >= 0 && index < Tiles.Count)
            {
                Tiles[index] = null;
            }
        }

        public Vector2 GetPositionOnScreen(int col, int row, Vector2 tileSize)
        {
            var screenX = tileSize.X * col;
            var screenY = tileSize.Y * row;

            return new Vector2(screenX, screenY);
        }

        public Vector2 GetPositionOnScreen(int col, int row)
        {
            return GetPositionOnScreen(col, row, this.TileSize);
        }

        public Vector2 GetPositionOnScreen(int index, Vector2 tileSize)
        {
            var pos = GetPosition(index);

            return GetPositionOnScreen((int)pos.X, (int)pos.Y, tileSize);
        }

        public Vector2 GetPositionOnScreen(int index)
        {
            return GetPositionOnScreen(index, this.TileSize);
        }

        public Vector2 GetPosition(int index)
        {
            var x = index % (int)LayerSize.X;
            var y = index / (int)LayerSize.X;

            return new Vector2(x, y);
        }

        public int GetIndex(int col, int row)
        {
            return (int)(row * LayerSize.X + col);
        }

        #endregion
    }
}
