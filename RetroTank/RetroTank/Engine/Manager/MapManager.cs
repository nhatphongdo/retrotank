﻿using System.Collections.Generic;
using System.Linq;
using GameAssetType;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using RetroTank.Engine.Object;

namespace RetroTank.Engine.Manager
{
    public static class MapManager
    {
        #region Properties

        public static int CurrentMap
        {
            get;
            private set;
        }

        public static Map Map
        {
            get;
            private set;
        }

        public static Dictionary<string, Texture2D> MapTextures
        {
            get;
            private set;
        }

        #endregion

        static MapManager()
        {
            CurrentMap = 1;
            MapTextures = new Dictionary<string, Texture2D>();
        }

        #region Public methods

        public static void NextMap()
        {
            ++CurrentMap;

            LoadMap();
        }

        public static void LoadMap()
        {
            Map = RetroTankGame.GetInstance().Content.Load<XmlNode>("Map/Map" + CurrentMap).ConvertTo<Map>();

            Map.TileSize = new Vector2((int)(RetroTankGame.GetInstance().PlayableRegion.Width / Map.MapSize.X),
                                       (int)(RetroTankGame.GetInstance().PlayableRegion.Height / Map.MapSize.Y));

            Map.Layers = Map.Layers.OrderBy(l => l.Index).ToList();

            // Load map's resources
            foreach (var layer in Map.Layers)
            {
                layer.Map = Map;

                Texture2D texture;
                if (MapTextures.ContainsKey(layer.LayerTexture.ToUpper()))
                {
                    texture = MapTextures[layer.LayerTexture.ToUpper()];
                }
                else
                {
                    texture = RetroTankGame.GetInstance().Content.Load<Texture2D>(layer.LayerTexture);
                    MapTextures.Add(layer.LayerTexture.ToUpper(), texture);
                }

                for (var idx = 0; idx < layer.Content.Count; idx++)
                {
                    if (layer.Content[idx] < 0)
                    {
                        layer.Tiles.Add(null);
                    }
                    else
                    {
                        var property = layer.Content[idx] < layer.TileProperties.Count ? layer.TileProperties[layer.Content[idx]] : 3;
                        var tile = new Tile(layer, texture, (property & 1) == 1, (property & 2) == 1)
                                       {
                                           Position = layer.GetPositionOnScreen(idx, Map.TileSize),
                                           Size = Map.TileSize,
                                           TextureSize = layer.TileSize,
                                           Offset = layer.GetPositionOnScreen(layer.Content[idx])
                                       };
                        layer.Tiles.Add(tile);
                    }
                }
            }

            // Create objects in map
            foreach (var flag in Map.Flags)
            {
                flag.Initialize(Map);
                ObjectManager.GameObjects.Add(flag);
            }
        }

        public static void Update(GameTime gameTime)
        {
            foreach (var layer in Map.Layers)
            {
                if (layer.Index > Map.GroundLayer)
                {
                    // Update objects after update all below layer
                    ObjectManager.Update(gameTime);
                }

                foreach (var tile in layer.Tiles.Where(tile => tile != null))
                {
                    tile.Update(gameTime);
                }
            }
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            foreach (var layer in Map.Layers)
            {
                if (layer.Index > Map.GroundLayer)
                {
                    // Draw objects after draw all below layer
                    ObjectManager.Draw(spriteBatch);
                }

                foreach (var tile in layer.Tiles.Where(tile => tile != null))
                {
                    tile.Draw(spriteBatch);
                }
            }
        }

        #endregion
    }
}
