﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using RetroTank.Engine.Manager;

namespace RetroTank.Engine.Object
{
    public class Bullet : ObjectBase
    {
        public readonly static Vector2 BulletSize = new Vector2(14, 14);

        public Tank Tank
        {
            get;
            private set;
        }

        private MapLayer _mapLayer;

        public Bullet(Tank tank)
        {
            Tank = tank;
            Texture = RetroTankGame.GetInstance().Content.Load<Texture2D>("Item/bullet");

            Speed = 10;
            Size = BulletSize;

            _mapLayer = Tank.Map.GetLayer(Tank.Map.GroundLayer);
        }

        #region Public methods

        public override void Update(GameTime time)
        {
            base.Update(time);

            // Check collision with the other objects
            var collision = Tank.Map.GetCollision(this);
            var tile = _mapLayer.GetTile(collision);
            if (tile != null && !tile.IsThroughable)
            {
                if (tile.IsBreakable)
                {
                    tile.Destroy();
                    _mapLayer.RemoveTile(collision);
                }

                Destroy();
            }

            for (var index = 0; index < ObjectManager.GameObjects.Count; index++)
            {
                var obj = ObjectManager.GameObjects[index];

                if (obj is Tank && Utility.IsCollision(Utility.GetBoundary(Position, Size), Utility.GetBoundary(obj.Position, obj.Size)))
                {
                    // Collision with tank
                    if (this.Tank.GetType() != obj.GetType())
                    {
                        obj.Destroy();
                        Destroy();
                        if (index >= 0 && index < ObjectManager.GameObjects.Count)
                        {
                            ObjectManager.GameObjects.RemoveAt(index);
                        }
                        break;
                    }
                }
                else if (obj is Flag && Utility.IsCollision(Utility.GetBoundary(Position, Size), Utility.GetBoundary(obj.Position, obj.Size)))
                {
                    RetroTankGame.GetInstance().Exit();
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            switch (MovingDirection)
            {
                case MovingDirection.Left:
                    // Draw left texture
                    spriteBatch.Draw(Texture, Utility.GetBoundary(Position + RetroTankGame.GetInstance().PlayableRegion.Location.ToVector2(), Size), new Rectangle(14 * 2, 0, 14, 14), Color.White);
                    break;
                case MovingDirection.Right:
                    // Draw right texture
                    spriteBatch.Draw(Texture, Utility.GetBoundary(Position + RetroTankGame.GetInstance().PlayableRegion.Location.ToVector2(), Size), new Rectangle(14 * 3, 0, 14, 14), Color.White);
                    break;
                case MovingDirection.Up:
                    // Draw up texture
                    spriteBatch.Draw(Texture, Utility.GetBoundary(Position + RetroTankGame.GetInstance().PlayableRegion.Location.ToVector2(), Size), new Rectangle(0, 0, 14, 14), Color.White);
                    break;
                case MovingDirection.Down:
                    // Draw down texture
                    spriteBatch.Draw(Texture, Utility.GetBoundary(Position + RetroTankGame.GetInstance().PlayableRegion.Location.ToVector2(), Size), new Rectangle(14, 0, 14, 14), Color.White);
                    break;
                default:
                    break;
            }
        }

        public override void Destroy()
        {
            for (var index = 0; index < ObjectManager.GameObjects.Count; index++)
            {
                if (ObjectManager.GameObjects[index] == this)
                {
                    ObjectManager.GameObjects.RemoveAt(index);
                    return;
                }
            }
        }

        public override void GoOutside(Vector2 oldPosition)
        {
            this.Destroy();
        }

        #endregion
    }
}
