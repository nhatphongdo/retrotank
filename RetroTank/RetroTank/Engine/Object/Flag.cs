﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RetroTank.Engine.Manager;

namespace RetroTank.Engine.Object
{
    public class Flag : ObjectBase
    {
        #region Properties

        public Vector2 FlagLocation
        {
            get;
            set;
        }

        public Vector2 FlagSize
        {
            get;
            set;
        }

        public string TextureName
        {
            get;
            set;
        }

        public Map Map
        {
            get;
            set;
        }

        #endregion

        public void Initialize(Map map)
        {
            Map = map;
            if (!string.IsNullOrEmpty(TextureName))
            {
                Texture = RetroTankGame.GetInstance().Content.Load<Texture2D>(TextureName);
            }
            Position = FlagLocation * Map.TileSize;
            Size = FlagSize * Map.TileSize;
        }

        #region Public methods

        public override void Update(Microsoft.Xna.Framework.GameTime time)
        {
            base.Update(time);

            // Find the shortest path to flag

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Utility.GetBoundary(Position + RetroTankGame.GetInstance().PlayableRegion.Location.ToVector2(), Size), new Rectangle(0, 0, 128, 128), Color.White);
        }

        public override void Destroy()
        {
            RetroTankGame.GetInstance().explosion.AddParticles(this.Position, Vector2.Zero);
            RetroTankGame.GetInstance().smoke.AddParticles(this.Position, Vector2.Zero);
        }

        #endregion
    }
}
