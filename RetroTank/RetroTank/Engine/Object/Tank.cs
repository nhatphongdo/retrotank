using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RetroTank.Engine.Manager;
using Microsoft.Xna.Framework.Input;

namespace RetroTank.Engine.Object
{
    public class Tank : ObjectBase
    {
        public Map Map
        {
            get;
            private set;
        }

        public Tank(Map map)
        {
            Map = map;
            Texture = RetroTankGame.GetInstance().Content.Load<Texture2D>("Character/tank");
        }

        #region Public methods

        public override void Update(GameTime gameTime)
        {
            var oldPosition = Position;

            base.Update(gameTime);

            // Update position to correct location
            // Find the closest tile in the same direction
            switch (MovingDirection)
            {
                case MovingDirection.RotateUp:
                    Position = _destination.HasValue && (Position.Y - Speed) >= _destination.Value.Y ? new Vector2(Position.X, Position.Y - Speed) : new Vector2(Position.X, (int)(Math.Round(Position.Y / Size.Y) * Size.Y));
                    break;
                case MovingDirection.RotateRight:
                    Position = _destination.HasValue && (Position.X + Speed) <= _destination.Value.X ? new Vector2(Position.X + Speed, Position.Y) : new Vector2((int)(Math.Round(Position.X / Size.X) * Size.X), Position.Y);
                    break;
                case MovingDirection.RotateDown:
                    Position = _destination.HasValue && (Position.Y + Speed) <= _destination.Value.Y ? new Vector2(Position.X, Position.Y + Speed) : new Vector2(Position.X, (int)(Math.Round(Position.Y / Size.Y) * Size.Y));
                    break;
                case MovingDirection.RotateLeft:
                    Position = _destination.HasValue && (Position.X - Speed) >= _destination.Value.X ? new Vector2(Position.X - Speed, Position.Y) : new Vector2((int)(Math.Round(Position.X / Size.X) * Size.X), Position.Y);
                    break;
            }

            // Check collision with the other objects
            if (Map.IsCollision(this))
            {
                // Return to correct position
                Position = oldPosition;
                Position = AdjustPosition();
                switch (MovingDirection)
                {
                    case MovingDirection.Up:
                    case MovingDirection.RotateUp:
                        if (Map.IsCollision(this))
                        {
                            Position = Position + new Vector2(0, Size.Y);
                        }
                        break;
                    case MovingDirection.Right:
                    case MovingDirection.RotateRight:
                        if (Map.IsCollision(this))
                        {
                            Position = Position - new Vector2(Size.X, 0);
                        }
                        break;
                    case MovingDirection.Down:
                    case MovingDirection.RotateDown:
                        if (Map.IsCollision(this))
                        {
                            Position = Position - new Vector2(0, Size.Y);
                        }
                        break;
                    case MovingDirection.Left:
                    case MovingDirection.RotateLeft:
                        if (Map.IsCollision(this))
                        {
                            Position = Position + new Vector2(Size.X, 0);
                        }
                        break;
                }
            }

            if(ObjectManager.IsCollision(this))
            {
                Position = oldPosition;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            switch (MovingDirection)
            {
                case MovingDirection.Left:
                case MovingDirection.RotateLeft:
                    // Draw left texture
                    spriteBatch.Draw(Texture, Utility.GetBoundary(Position + RetroTankGame.GetInstance().PlayableRegion.Location.ToVector2(), Size), new Rectangle(64 * 2, 0, 64, 64), Color.White);
                    break;
                case MovingDirection.Right:
                case MovingDirection.RotateRight:
                    // Draw right texture
                    spriteBatch.Draw(Texture, Utility.GetBoundary(Position + RetroTankGame.GetInstance().PlayableRegion.Location.ToVector2(), Size), new Rectangle(64 * 3, 0, 64, 64), Color.White);
                    break;
                case MovingDirection.Up:
                case MovingDirection.RotateUp:
                    // Draw up texture
                    spriteBatch.Draw(Texture, Utility.GetBoundary(Position + RetroTankGame.GetInstance().PlayableRegion.Location.ToVector2(), Size), new Rectangle(0, 0, 64, 64), Color.White);
                    break;
                case MovingDirection.Down:
                case MovingDirection.RotateDown:
                    // Draw down texture
                    spriteBatch.Draw(Texture, Utility.GetBoundary(Position + RetroTankGame.GetInstance().PlayableRegion.Location.ToVector2(), Size), new Rectangle(64, 0, 64, 64), Color.White);
                    break;
                default:
                    break;
            }
        }

        public override void ProcessKeyboard(KeyboardState keyboardState, KeyboardState previousKeyboardState)
        {
            var hasAction = false;
            foreach (var currentKey in keyboardState.GetPressedKeys())
            {
                switch (currentKey)
                {
                    case Keys.Left:
                        hasAction = true;
                        MovingDirection = MovingDirection.Left;
                        break;
                    case Keys.Right:
                        hasAction = true;
                        MovingDirection = MovingDirection.Right;
                        break;
                    case Keys.Up:
                        hasAction = true;
                        MovingDirection = MovingDirection.Up;
                        break;
                    case Keys.Down:
                        hasAction = true;
                        MovingDirection = MovingDirection.Down;
                        break;
                    case Keys.Space:
                        if (previousKeyboardState.IsKeyUp(Keys.Space))
                        {
                            hasAction = true;
                            Fire();
                        }
                        break;
                    default:
                        break;
                }
            }

            if (!hasAction)
            {
                switch (MovingDirection)
                {
                    case MovingDirection.Left:
                        MovingDirection = MovingDirection.RotateLeft;
                        break;
                    case MovingDirection.Right:
                        MovingDirection = MovingDirection.RotateRight;
                        break;
                    case MovingDirection.Up:
                        MovingDirection = MovingDirection.RotateUp;
                        break;
                    case MovingDirection.Down:
                        MovingDirection = MovingDirection.RotateDown;
                        break;
                    default:
                        break;
                }
            }
        }

        public void Fire()
        {
            var shiftOffsetX = (Size.X - Bullet.BulletSize.X) / 2;
            var shiftOffsetY = (Size.Y - Bullet.BulletSize.Y) / 2;

            var bullet = new Bullet(this);

            switch (MovingDirection)
            {
                case MovingDirection.RotateDown:
                case MovingDirection.Down:
                    bullet.MovingDirection = MovingDirection.Down;
                    bullet.Position = Position.ShiftX(shiftOffsetX).ShiftY(Size.Y / 2 + Bullet.BulletSize.Y);
                    break;
                case MovingDirection.RotateUp:
                case MovingDirection.Up:
                    bullet.MovingDirection = MovingDirection.Up;
                    bullet.Position = Position.ShiftX(shiftOffsetX).ShiftY(-Size.Y / 2 + Bullet.BulletSize.Y);
                    break;
                case MovingDirection.RotateLeft:
                case MovingDirection.Left:
                    bullet.MovingDirection = MovingDirection.Left;
                    bullet.Position = Position.ShiftY(shiftOffsetY).ShiftX(-Size.X / 2 + Bullet.BulletSize.X);
                    break;
                case MovingDirection.RotateRight:
                case MovingDirection.Right:
                    bullet.MovingDirection = MovingDirection.Right;
                    bullet.Position = Position.ShiftY(shiftOffsetY).ShiftX(Size.X / 2 + Bullet.BulletSize.X);
                    break;
            }

            ObjectManager.GameObjects.Add(bullet);
        }

        public override void Destroy()
        {
            RetroTankGame.GetInstance().explosion.AddParticles(this.Position, Vector2.Zero);
            RetroTankGame.GetInstance().smoke.AddParticles(this.Position, Vector2.Zero);
        }

        #endregion
    }
}
