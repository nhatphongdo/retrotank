﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RetroTank.Engine.Manager;

namespace RetroTank.Engine.Object
{
    public class EnemyTank : Tank
    {
        public enum Actions
        {
            GoStraight,
            GoBack,
            TurnLeft,
            TurnRight,
            Fire
        }

        #region Private variables

        private GameTime _lastFire;

        private readonly Random _random = new Random((int)DateTime.Now.Ticks);

        private int[,] _visitedTiles;

        private Vector2 _lastLocation = new Vector2(-1, -1);

        #endregion

        #region Properties

        public int Type
        {
            get;
            set;
        }

        public int Sight
        {
            get;
            set;
        }

        #endregion

        public EnemyTank(Map map, int type)
            : base(map)
        {
            Type = type;
            switch (Type)
            {
                case 1:
                    Texture = RetroTankGame.GetInstance().Content.Load<Texture2D>("Enemy/enemy1");
                    break;
                case 2:
                    Texture = RetroTankGame.GetInstance().Content.Load<Texture2D>("Enemy/enemy2");
                    break;
                case 3:
                    Texture = RetroTankGame.GetInstance().Content.Load<Texture2D>("Enemy/enemy3");
                    break;
                case 4:
                    Texture = RetroTankGame.GetInstance().Content.Load<Texture2D>("Enemy/enemy4");
                    break;
            }
            Sight = 10;

            var location = _random.Next(0, Map.EnemyStartLocations.Count);
            Position = Map.EnemyStartLocations[location] * Map.TileSize;

            var direction = _random.Next(0, 8);
            MovingDirection = (MovingDirection)Enum.Parse(typeof(MovingDirection), direction.ToString(), false);

            _visitedTiles = new int[(int)Map.GetLayer(Map.GroundLayer).TileSize.Y, (int)Map.GetLayer(Map.GroundLayer).TileSize.X];
        }

        #region Internal methods

        private void DecideToFire(GameTime gameTime)
        {
            var rand = _random.Next(100);

            if (rand % 7 == 0)
            {
                Fire();
            }
        }

        private bool CanTurnLeft(Vector2 location, out Vector2 newLocation)
        {
            switch (MovingDirection)
            {
                case MovingDirection.RotateLeft:
                case MovingDirection.Left:
                    newLocation = new Vector2((int)location.X, (int)location.Y + 1);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
                case MovingDirection.RotateRight:
                case MovingDirection.Right:
                    newLocation = new Vector2((int)location.X, (int)location.Y - 1);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
                case MovingDirection.RotateUp:
                case MovingDirection.Up:
                    newLocation = new Vector2((int)location.X - 1, (int)location.Y);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
                case MovingDirection.RotateDown:
                case MovingDirection.Down:
                    newLocation = new Vector2((int)location.X + 1, (int)location.Y);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
            }

            newLocation = location;
            return false;
        }

        private bool CanTurnRight(Vector2 location, out Vector2 newLocation)
        {
            switch (MovingDirection)
            {
                case MovingDirection.RotateLeft:
                case MovingDirection.Left:
                    newLocation = new Vector2((int)location.X, (int)location.Y - 1);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
                case MovingDirection.RotateRight:
                case MovingDirection.Right:
                    newLocation = new Vector2((int)location.X, (int)location.Y + 1);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
                case MovingDirection.RotateUp:
                case MovingDirection.Up:
                    newLocation = new Vector2((int)location.X + 1, (int)location.Y);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
                case MovingDirection.RotateDown:
                case MovingDirection.Down:
                    newLocation = new Vector2((int)location.X - 1, (int)location.Y);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
            }

            newLocation = location;
            return false;
        }

        private bool CanGoStraight(Vector2 location, out Vector2 newLocation)
        {
            switch (MovingDirection)
            {
                case MovingDirection.RotateLeft:
                case MovingDirection.Left:
                    newLocation = new Vector2((int)location.X - 1, (int)location.Y);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
                case MovingDirection.RotateRight:
                case MovingDirection.Right:
                    newLocation = new Vector2((int)location.X + 1, (int)location.Y);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
                case MovingDirection.RotateUp:
                case MovingDirection.Up:
                    newLocation = new Vector2((int)location.X, (int)location.Y - 1);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
                case MovingDirection.RotateDown:
                case MovingDirection.Down:
                    newLocation = new Vector2((int)location.X, (int)location.Y + 1);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
            }

            newLocation = location;
            return false;
        }

        private bool CanGoBack(Vector2 location, out Vector2 newLocation)
        {
            switch (MovingDirection)
            {
                case MovingDirection.RotateLeft:
                case MovingDirection.Left:
                    newLocation = new Vector2((int)location.X + 1, (int)location.Y);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
                case MovingDirection.RotateRight:
                case MovingDirection.Right:
                    newLocation = new Vector2((int)location.X - 1, (int)location.Y);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
                case MovingDirection.RotateUp:
                case MovingDirection.Up:
                    newLocation = new Vector2((int)location.X, (int)location.Y + 1);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
                case MovingDirection.RotateDown:
                case MovingDirection.Down:
                    newLocation = new Vector2((int)location.X, (int)location.Y - 1);
                    return Map.CanGo((int)newLocation.Y, (int)newLocation.X);
            }

            newLocation = location;
            return false;
        }

        #endregion

        #region Public methods

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // Process AI
            var tilePosition = new Vector2(Position.X / Map.TileSize.X, Position.Y / Map.TileSize.Y);

            if (tilePosition != _lastLocation)
            {
                _visitedTiles[(int)tilePosition.Y, (int)tilePosition.X] += 1;
                _lastLocation = tilePosition;
            }

            // Move to next point
            var posibleActions = new List<Actions>();
            Actions? action = null;
            var weightVisiting = int.MaxValue;
            Vector2 newLocation;
            if (CanGoStraight(tilePosition, out newLocation))
            {
                if (weightVisiting > _visitedTiles[(int)newLocation.Y, (int)newLocation.X])
                {
                    action = Actions.GoStraight;
                    weightVisiting = _visitedTiles[(int)newLocation.Y, (int)newLocation.X];
                }
            }
            if (CanTurnRight(tilePosition, out newLocation))
            {
                if (weightVisiting > _visitedTiles[(int)newLocation.Y, (int)newLocation.X])
                {
                    action = Actions.TurnRight;
                    weightVisiting = _visitedTiles[(int)newLocation.Y, (int)newLocation.X];
                }
            }
            if (CanTurnLeft(tilePosition, out newLocation))
            {
                if (weightVisiting > _visitedTiles[(int)newLocation.Y, (int)newLocation.X])
                {
                    action = Actions.TurnLeft;
                    weightVisiting = _visitedTiles[(int)newLocation.Y, (int)newLocation.X];
                }
            }
            if (CanGoBack(tilePosition, out newLocation))
            {
                if (weightVisiting > _visitedTiles[(int)newLocation.Y, (int)newLocation.X])
                {
                    action = Actions.GoBack;
                    weightVisiting = _visitedTiles[(int)newLocation.Y, (int)newLocation.X];
                }
            }

            if (action.HasValue)
            {
                posibleActions.Add(action.Value);
            }

            if (posibleActions.Count > 0)
            {
                var actionIndex = _random.Next(0, posibleActions.Count);
                var nextDirection = 0;
                switch (posibleActions[actionIndex])
                {
                    case Actions.GoStraight:
                        nextDirection = (int)MovingDirection + 4;
                        if (nextDirection > 7)
                        {
                            nextDirection -= 4;
                        }
                        break;
                    case Actions.GoBack:
                        nextDirection = (int)MovingDirection + 2;
                        if ((nextDirection > 3 && (int)MovingDirection < 4) ||
                            (nextDirection > 7 && (int)MovingDirection >= 4))
                        {
                            nextDirection -= 4;
                        }
                        break;
                    case Actions.TurnLeft:
                        nextDirection = ((int)MovingDirection) - 1;
                        if ((nextDirection < 0 && (int)MovingDirection < 4) ||
                            (nextDirection < 4 && (int)MovingDirection >= 4))
                        {
                            nextDirection += 4;
                        }
                        break;
                    case Actions.TurnRight:
                        nextDirection = ((int)MovingDirection) + 1;
                        if ((nextDirection > 3 && (int)MovingDirection < 4) ||
                            (nextDirection > 7 && (int)MovingDirection >= 4))
                        {
                            nextDirection -= 4;
                        }
                        break;
                    case Actions.Fire:
                        Fire();
                        break;
                }

                MovingDirection = (MovingDirection)Enum.Parse(typeof(MovingDirection), nextDirection.ToString(), false);
            }
        }

        public override void ProcessKeyboard(KeyboardState keyboardState, KeyboardState previousKeyboardState)
        {
        }

        #endregion
    }
}
