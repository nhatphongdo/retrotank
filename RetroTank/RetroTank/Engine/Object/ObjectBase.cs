using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RetroTank.Engine.Manager;
using Microsoft.Xna.Framework.Input;

namespace RetroTank.Engine.Object
{
    public abstract class ObjectBase
    {
        #region Properties

        public Vector2 Position
        {
            get;
            set;
        }

        public Vector2 Size
        {
            get;
            set;
        }

        public Vector2 Offset
        {
            get;
            set;
        }

        public Texture2D Texture
        {
            get;
            protected set;
        }

        public MovingDirection MovingDirection
        {
            get;
            set;
        }

        public float Speed
        {
            get;
            set;
        }

        #endregion

        #region Private variables

        protected bool _isMoving = false;

        protected Vector2? _destination = null;

        #endregion

        protected ObjectBase()
        {
            Texture = null;
            Position = Vector2.Zero;
            Offset = Vector2.Zero;
            Size = new Vector2(64, 64);
            Speed = 7;
        }

        protected ObjectBase(Texture2D texture)
        {
            Texture = texture;
            Position = Vector2.Zero;
        }

        public virtual void Update(GameTime time)
        {
            var oldPosition = Position;

            switch (MovingDirection)
            {
                case MovingDirection.Left:
                    Position = new Vector2(Position.X - Speed, Position.Y);
                    _isMoving = true;
                    break;
                case MovingDirection.Right:
                    Position = new Vector2(Position.X + Speed, Position.Y);
                    _isMoving = true;
                    break;
                case MovingDirection.Up:
                    Position = new Vector2(Position.X, Position.Y - Speed);
                    _isMoving = true;
                    break;
                case MovingDirection.Down:
                    Position = new Vector2(Position.X, Position.Y + Speed);
                    _isMoving = true;
                    break;
                default:
                    // Stand
                    if (_isMoving)
                    {
                        _destination = AdjustPosition();
                        _isMoving = false;
                    }
                    break;
            }

            if (!Utility.IsInside(Utility.GetBoundary(Position + RetroTankGame.GetInstance().PlayableRegion.Location.ToVector2(), Size), RetroTankGame.GetInstance().PlayableRegion))
            {
                // Return old value if object runs outside the playable region
                GoOutside(oldPosition);
            }
        }

        public virtual void ProcessKeyboard(KeyboardState keyboardState, KeyboardState previousKeyboardState)
        {
        }

        public abstract void Draw(SpriteBatch spriteBatch);

        public virtual void DrawRegion(SpriteBatch spriteBatch)
        {
            var rectTexture = new Texture2D(RetroTankGame.GetInstance().GraphicsDevice, (int)Size.X, (int)Size.Y);
            var pixels = new uint[rectTexture.Width * rectTexture.Height];
            var color = new Color(0, 0, 255);
            for (var x = 0; x < rectTexture.Width; x++)
            {
                pixels[x] = color.PackedValue;
                pixels[(rectTexture.Height - 1) * rectTexture.Width + x] = color.PackedValue;
            }
            for (var y = 0; y < rectTexture.Height; y++)
            {
                pixels[y * rectTexture.Width] = color.PackedValue;
                pixels[(y + 1) * rectTexture.Width - 1] = color.PackedValue;
            }
            rectTexture.SetData<uint>(pixels);
            spriteBatch.Draw(rectTexture, this.Position, null, Color.White, 0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0f);
        }

        public virtual void Destroy()
        {
        }

        public virtual void GoOutside(Vector2 oldPosition)
        {
            this.Position = oldPosition;
        }

        public Vector2 AdjustPosition()
        {
            switch (MovingDirection)
            {
                case MovingDirection.Left:
                case MovingDirection.RotateLeft:
                    return new Vector2((int)(Math.Floor(Position.X / Size.X) * Size.X), (int)(Math.Round(Position.Y / Size.Y) * Size.Y));
                case MovingDirection.Right:
                case MovingDirection.RotateRight:
                    return new Vector2((int)(Math.Ceiling(Position.X / Size.X) * Size.X), (int)(Math.Round(Position.Y / Size.Y) * Size.Y));
                case MovingDirection.Up:
                case MovingDirection.RotateUp:
                    return new Vector2((int)(Math.Round(Position.X / Size.X) * Size.X), (int)(Math.Floor(Position.Y / Size.Y) * Size.Y));
                case MovingDirection.Down:
                case MovingDirection.RotateDown:
                    return new Vector2((int)(Math.Round(Position.X / Size.X) * Size.X), (int)(Math.Ceiling(Position.Y / Size.Y) * Size.Y));
            }

            return Position;
        }
    }
}
