﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RetroTank.Engine.Manager;

namespace RetroTank.Engine.Object
{
    public class Tile : ObjectBase
    {
        #region Properties

        public bool IsBreakable
        {
            get;
            set;
        }

        public bool IsThroughable
        {
            get;
            set;
        }

        public int Health
        {
            get;
            set;
        }

        public MapLayer MapLayer
        {
            get;
            private set;
        }

        public Vector2 TextureSize
        {
            get;
            set;
        }

        #endregion

        public Tile(MapLayer layer, string assetName)
            : this(layer, assetName, true, false)
        {
        }

        public Tile(MapLayer layer, string assetName, bool isBreakable, bool isThroughable)
        {
            this.MapLayer = layer;

            IsBreakable = isBreakable;
            IsThroughable = isThroughable;
            // Load texture corresponding to type
            Texture = RetroTankGame.GetInstance().Content.Load<Texture2D>(assetName);
            TextureSize = new Vector2(64, 64);
        }

        public Tile(MapLayer layer, Texture2D asset)
            : this(layer, asset, true, false)
        {
        }

        public Tile(MapLayer layer, Texture2D asset, bool isBreakable, bool isThroughable)
        {
            this.MapLayer = layer;

            IsBreakable = isBreakable;
            IsThroughable = isThroughable;
            // Load texture corresponding to type
            Texture = asset;
            TextureSize = new Vector2(64, 64);
        }

        #region Public methods

        public override void Update(GameTime time)
        {
            base.Update(time);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Texture != null)
            {
                spriteBatch.Draw(Texture, Utility.GetBoundary(Position + RetroTankGame.GetInstance().PlayableRegion.Location.ToVector2(), Size), Utility.GetBoundary(Offset, TextureSize), Color.White);
            }
        }

        public override void Destroy()
        {
            IsBreakable = false;
            IsThroughable = true;
            Texture = null;
        }

        #endregion
    }
}
