﻿using System;

namespace RetroTank
{
#if WINDOWS_PHONE || WINDOWS || XBOX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            using (var game = new RetroTankGame())
            {
                game.Run();
            }
        }
    }
#else
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var factory = new MonoGame.Framework.GameFrameworkViewSource<RetroTankGame>();
            Windows.ApplicationModel.Core.CoreApplication.Run(factory);
        }
    }
#endif
}
