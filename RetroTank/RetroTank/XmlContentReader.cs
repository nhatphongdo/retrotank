﻿using System.IO;
using GameAssetType;
using Microsoft.Xna.Framework.Content;

namespace VietDev.Xna
{
    public class XmlContentReader : ContentTypeReader<XmlNode>
    {
        protected override XmlNode Read(ContentReader input, XmlNode existingInstance)
        {
            var buffer = input.ReadBytes((int)input.BaseStream.Length);
            var stream = new MemoryStream(buffer);

            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(XmlNode));
            var result = serializer.Deserialize(stream) as XmlNode;

            return result;
        }
    }
}
