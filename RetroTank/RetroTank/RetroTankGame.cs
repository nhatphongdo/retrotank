using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using RetroTank.Engine.Effect;
using RetroTank.Engine.Manager;
using RetroTank.Engine.Object;

namespace RetroTank
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class RetroTankGame : Microsoft.Xna.Framework.Game
    {
        private static RetroTankGame _singletonObject;

        private KeyboardState _previousKeyboardState;

        private GraphicsDeviceManager graphics;
        private SpriteBatch _spriteBatch;

        internal ParticleSystem explosion;
        internal ParticleSystem smoke;

        public Rectangle PlayableRegion
        {
            get;
            private set;
        }

        public RetroTankGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // Frame rate is 30 fps by default for Windows Phone.
            TargetElapsedTime = TimeSpan.FromTicks(333333);

#if !WINDOWS
            graphics.IsFullScreen = true;
#endif

#if WINDOWS
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
#endif

            // Extend battery life under lock.
            InactiveSleepTime = TimeSpan.FromSeconds(1);

            // Set playable region
            PlayableRegion = new Rectangle((graphics.PreferredBackBufferWidth - graphics.PreferredBackBufferHeight) / 2, 0, graphics.PreferredBackBufferHeight, graphics.PreferredBackBufferHeight);

            _singletonObject = this;


            // create the particle systems and add them to the components list.
            explosion = new ParticleSystem(this, "Effect/ExplosionSettings")
                            {
                                DrawOrder = ParticleSystem.AdditiveDrawOrder
                            };
            Components.Add(explosion);

            smoke = new ParticleSystem(this, "Effect/ExplosionSmokeSettings")
                        {
                            DrawOrder = ParticleSystem.AlphaBlendDrawOrder
                        };
            Components.Add(smoke);
        }

        public static RetroTankGame GetInstance()
        {
            return _singletonObject ?? (_singletonObject = new RetroTankGame());
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            _previousKeyboardState = Keyboard.GetState();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            MapManager.LoadMap();
            ObjectManager.GameObjects.Add(new Tank(MapManager.Map)
                                              {
                                                  Position = MapManager.Map.PlayerStartLocations[0] * MapManager.Map.TileSize,
                                                  Size = MapManager.Map.TileSize,
                                                  MovingDirection = MovingDirection.RotateUp,
                                                  Speed = MapManager.Map.TileSize.X / 10f
                                              });
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here

            // Process key
            UpdateInput(gameTime);

            // Add enemy if neccessary
            if (ObjectManager.GameObjects.Count < 3)
            {
                ObjectManager.GameObjects.Add(new EnemyTank(MapManager.Map, 1)
                                                  {
                                                      Size = MapManager.Map.TileSize,
                                                      Speed = MapManager.Map.TileSize.X / 10f
                                                  });
            }

            MapManager.Update(gameTime);

            base.Update(gameTime);
        }

        protected void UpdateInput(GameTime gameTime)
        {
            var keyboardState = Keyboard.GetState();

            ObjectManager.ProcessKeyboard(keyboardState, _previousKeyboardState);

            _previousKeyboardState = keyboardState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            _spriteBatch.Begin();

            MapManager.Draw(_spriteBatch);

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
