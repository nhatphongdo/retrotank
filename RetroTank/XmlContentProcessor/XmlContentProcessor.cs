using Microsoft.Xna.Framework.Content.Pipeline;
using Xml = System.Xml;
using GameAssetType;

// TODO: replace these with the processor input and output types.
using TInput = System.String;
using TOutput = GameAssetType.XmlNode;

namespace XmlContentProcessor
{
    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to apply custom processing to content data, converting an object of
    /// type TInput to TOutput. The input and output types may be the same if
    /// the processor wishes to alter data without changing its type.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    ///
    /// TODO: change the ContentProcessor attribute to specify the correct
    /// display name for this processor.
    /// </summary>
    [ContentProcessor(DisplayName = "Xml Content Processor")]
    public class XmlContentProcessor : ContentProcessor<TInput, TOutput>
    {
        public override TOutput Process(TInput input, ContentProcessorContext context)
        {
            var output = new XmlNode();

            var xmlDocument = new Xml.XmlDocument();
            xmlDocument.LoadXml(input);

            foreach (var node in xmlDocument.ChildNodes)
            {
                if ((node as Xml.XmlNode).NodeType == Xml.XmlNodeType.Element)
                {
                    // This is the root
                    output = ProcessNode(node as Xml.XmlNode);
                }
            }

            return output;
        }

        private XmlNode ProcessNode(Xml.XmlNode originalNode)
        {
            var node = new XmlNode();
            node.Name = originalNode.Name;

            switch (originalNode.NodeType)
            {
                case Xml.XmlNodeType.Comment:
                    // Ignore comment
                    return null;
                case Xml.XmlNodeType.Element:
                    node.NodeType = XmlNodeType.Element;

                    foreach (var attribute in originalNode.Attributes)
                    {
                        node.Attributes.Add(new XmlAttribute()
                        {
                            Name = (attribute as Xml.XmlAttribute).Name,
                            Value = (attribute as Xml.XmlAttribute).Value
                        });
                    }

                    foreach (var child in originalNode.ChildNodes)
                    {
                        var childNode = ProcessNode(child as Xml.XmlNode);
                        if (childNode != null)
                        {
                            node.ChildNodes.Add(childNode);
                        }
                    }

                    return node;
                case Xml.XmlNodeType.Text:
                    node.NodeType = XmlNodeType.Text;
                    node.Value = originalNode.Value;
                    return node;
                default:
                    return null;
            }
        }
    }
}