using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

// TODO: replace this with the type you want to write out.
using TWrite = GameAssetType.XmlNode;
using System.IO;

namespace XmlContentProcessor
{
    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to write the specified data type into binary .xnb format.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    /// </summary>
    [ContentTypeWriter]
    public class XmlContentWriter : ContentTypeWriter<TWrite>
    {
        protected override void Write(ContentWriter output, TWrite value)
        {
            // TODO: write the specified value to the output ContentWriter.
            var streamBuffer = new MemoryStream();
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(TWrite));
            serializer.Serialize(streamBuffer, value);

            output.Write(streamBuffer.ToArray());
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            // TODO: change this to the name of your ContentTypeReader
            // class which will be used to load this data.
            return "VietDev.Xna.XmlContentReader, RetroTank";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return typeof(GameAssetType.XmlNode).AssemblyQualifiedName;
        }
    }
}
