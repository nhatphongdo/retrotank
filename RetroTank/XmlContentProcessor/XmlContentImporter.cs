﻿using Microsoft.Xna.Framework.Content.Pipeline;

namespace XmlContentProcessor
{
    [ContentImporter(".xml", DisplayName = "XML Content Importer")]
    public class XmlContentImporter : ContentImporter<string>
    {
        public override string Import(string filename, ContentImporterContext context)
        {
            var content = System.IO.File.ReadAllText(filename);

            return content;
        }
    }
}
